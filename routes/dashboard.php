<?php

use App\Http\Controllers\Dashboard\Auth\LoginController;
use App\Http\Controllers\Dashboard\Companies\CompaniesController;
use App\Http\Controllers\Dashboard\Employees\EmployeesController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Dashboard Routes
|--------------------------------------------------------------------------
|
| Here is where you can register dashboard routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

# Login routes.
Route::get('login', [LoginController::class,'showLoginForm'])->name('login');
Route::post('login', [LoginController::class,'login'])->name('login');
Route::any('logout', [LoginController::class,'logout'])->name('logout');

# Dashboard group routes.
Route::group(['middleware' => 'auth'], function () {

    # dashboard route.
    Route::get('/', function () {
        return view('dashboard.home.home');
    });

    # companies routes.
    Route::resource('companies', CompaniesController::class)->except('show');

    # employees routes.
    Route::resource('employees', EmployeesController::class)->except('show');
});
