@extends('dashboard.layouts.app')

@section('title', 'Employees')

@section('content')
    <div class="container-fluid">

        <!-- Page Heading -->
        <h1 class="h3 mb-4 text-gray-800">Employees</h1>

        <!-- BEGIN :: alert session -->
        @include('dashboard.components.alert-session')
        <!-- END :: alert session -->

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary float-left">Employees Table</h6>

                <button type="button" class="btn btn-outline-primary float-right" onclick="location.href='{{ url('admin/employees/create') }}'">
                    <i class="fa fa-plus"></i>
                    Create
                </button>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Full Name</th>
                            <th>Company</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>website</th>
                            <th>Tools</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>Full Name</th>
                            <th>Company</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>website</th>
                            <th>Tools</th>
                        </tr>
                        </tfoot>
                        <tbody>
                        @forelse($employees as $employee)
                            <tr>
                                <td>{{ $employee->fullName }}</td>
                                <td>{{ $employee->company->name }}</td>
                                <td>{{ $employee->email }}</td>
                                <td>{{ $employee->phone }}</td>
                                <td>{{ $employee->website }}</td>
                                <td>
                                    <a class="btn btn-primary" href="{{ url("admin/employees/$employee->id/edit") }}" role="button">
                                        <i class="fa fa-pencil"></i>
                                    </a>

                                    <button class="btn btn-danger"
                                            data-toggle="modal"
                                            data-target="#deleteModal"
                                            data-url="{{ url("admin/employees/$employee->id") }}"
                                            id="delete-employee"
                                            role="button">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="2"></td>
                                <td colspan="2">Not Found Data</td>
                                <td colspan="2"></td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
                {{ $employees->links() }}
            </div>
        </div>
    </div>

    <!-- Begin : include delete modal -->
    @include('dashboard.components.delete-modal', [
        'form_id' => 'delete-employee-form',
        'delete_title' => 'Employee',
        'btn_delete_id' => '#delete-employee'
    ])
    <!-- End : include delete modal -->
@stop
