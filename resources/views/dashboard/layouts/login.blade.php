<!DOCTYPE html>
<html lang="en">

<!-- BEGIN :: include head -->
@include('dashboard.layouts.particles.head')
<!-- END :: include head -->

<body class="bg-gradient-primary">

<div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">

        <div class="col-xl-10 col-lg-12 col-md-9">

            @yield('content')

        </div>

    </div>

</div>

<!-- BEGIN :: include scripts -->
@include('dashboard.layouts.particles.scripts')
<!-- END :: include scripts -->
</body>
</html>
