<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <form method="post" action="#" id="{{ $form_id }}">
            @csrf
            {{ method_field('DELETE') }}
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"> Delete {{ $delete_title }}</h5>
                </div>
                <div class="modal-body">
                    <p>delete this!</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">close</button>
                    <button type="submit" class="btn btn-danger">delete</button>
                </div>
            </div>
        </form>
    </div>
</div>

@push('js')
    <!-- Begin :: Script To Change Modal Delete -->
    <script type="text/javascript">
        $(document).on('click', "{{ $btn_delete_id }}", function () {
            $("#{{ $form_id }}").attr('action', $(this).data('url'));
        })
    </script>
    <!-- End :: Script To Change Modal Delete -->
@endpush
