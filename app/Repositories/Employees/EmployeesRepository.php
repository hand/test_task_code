<?php


namespace App\Repositories\Employees;


use App\Enums\GeneralEnums;
use App\Models\Employee;
use App\Repositories\EloquentRepositoryInterface;
use App\Repositories\Repository;
use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\Collection;

class EmployeesRepository extends Repository implements EloquentRepositoryInterface
{
    public function __construct(Employee $model)
    {
        parent::__construct($model);
    }

    /**
     * List model.
     *
     * @param int $perPage
     * @return Collection
     */
    public function get($perPage = GeneralEnums::perPage)
    {
        return $this->model->with('company')->paginate($perPage);
    }

    /**
     * Get specification model by key.
     *
     * @param int $key
     * @return Model
     */
    public function find(int $key): Model
    {
        return $this->model->where('id', $key)->with('image')->first();
    }

    /**
     * Create new model.
     *
     * @param array $data
     * @return Model
     */
    public function create(array $data): Model
    {
        return $this->model->create($data)->image()->create(['image' => $data['image']->store('employees', 'public')]);
    }

    /**
     * Update model.
     *
     * @param Model $model
     * @param array $data
     * @return bool
     */
    public function update(Model $model, array $data): bool
    {
        return $model->update($data);
    }

    /**
     * Delete model.
     *
     * @param Model $model
     * @return bool
     * @throws \Exception
     */
    public function delete(Model $model): bool
    {
        return $model->delete();
    }
}
