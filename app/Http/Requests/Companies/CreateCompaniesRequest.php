<?php

namespace App\Http\Requests\Companies;

use App\Enums\GeneralEnums;
use Illuminate\Foundation\Http\FormRequest;

class CreateCompaniesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'image' => ['required', 'image', 'dimensions:min_width=200,min_height=200', 'max:3000', 'mimes:' . GeneralEnums::mimesType],
            'name' => ['required', 'max:70'],
            'email' => ['required', 'max:100', 'email', 'unique:companies,email'],
            'website' => ['required', 'max:100', 'url', 'regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/'],
        ];
    }
}
