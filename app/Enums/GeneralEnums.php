<?php


namespace App\Enums;


class GeneralEnums
{
    # for paginate page count.
    const perPage = 10;

    const mimesType = 'png,jpg,jpeg';
}
